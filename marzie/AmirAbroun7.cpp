// By Amir Abroun
#include <iostream>
using namespace std;

int main()
{
    int x, y, h;

    cout << "x: ";
    cin >> x;

    cout << "y: ";
    cin >> y;

    cout << "h: ";
    cin >> h;

    cout << "Area: " << ((x + y) * h) / 2;

    return 0;
}
