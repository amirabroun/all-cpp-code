#include <iostream>
using namespace std;

string Compound(int);

int main()
{
    cout << "\n";
    char qustion = 'y';
    do
    {
        cout << "enter number: ";
        int num;
        cin >> num;

        if (num == 1)
        {
            cout << " no Prime number and no Compound number" << endl;
        }
        else
        {
            cout << Compound(num);
        }

        cout << "\n";
        cout << "return <y/n>: ";
        cin >> qustion;
    } while (qustion == 'y');
}

string Compound(int n)
{
    for (int j = 2; j < n; j++)
    {
        if (n % j == 0)
        {
            return " Compound number";
        }
    }
    return " Prime number";
}