// BY Amir Abroun
#include <iostream>

using namespace std;
int main()
{
    int n;
    cout << "N*N. N: ";
    cin >> n;

    int Total[n][n];
    int Submission[n][n];
    int Multiplication[n][n];

    cout << "\narr1: \n";
    int arr1[n][n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> arr1[i][j];
        }
    }

    cout << "\narr2: \n";
    int arr2[n][n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> arr2[i][j];
        }
    }

    // total and Submission
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            Total[i][j] = arr1[i][j] + arr2[i][j];
            Submission[i][j] = arr1[i][j] - arr2[i][j];
        }
    }
    // Multiplication
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            Multiplication[i][j] = 0;
            for (int k = 0; k < n; k++)
            {
                Multiplication[i][j] += (arr1[i][k] * arr2[k][j]);
            }
        }
    }

    cout << "\nTotal: " << endl;
    for (int i = 0; i < n; i++)
    {
        cout << endl;
        for (int j = 0; j < n; j++)
        {
            cout << "   " << Total[i][j];
        }
        cout << endl;
    }
    cout << "\n\n";

    cout << "Submission: " << endl;
    for (int i = 0; i < n; i++)
    {
        cout << endl;
        for (int j = 0; j < n; j++)
        {
            cout << "   " << Submission[i][j];
        }
        cout << endl;
    }

    cout << "\nMultiple: " << endl;
    for (int i = 0; i < n; i++)
    {
        cout << endl;
        for (int j = 0; j < n; j++)
            cout << "   " << Multiplication[i][j];
        cout << endl;
    }

    return 0;
}