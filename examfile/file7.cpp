#include <fstream>
#include <iostream>
using namespace std;

int main()
{
    int b[3] = {10, 34, 78};

    ofstream f1("file.b", ios::binary);
    f1.write((char *)b, 12);
    f1.close();

    ifstream f2("file.b", ios::binary);
    f2.read((char *)b, 12);

    for (int i = 0; i < 3; i++)
    {
        cout << b[i] << " ";
    }

    return 0;
}