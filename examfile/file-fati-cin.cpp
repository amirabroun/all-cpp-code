#include <fstream>
#include <iostream>
#include <string>

using namespace std;

int main()
{
    int i, day, sales;

    ofstream fout("amir.txt");
    if (!fout)
    {
        cerr << "File not open!" << endl;
        exit(0);
    }
    cout << "Enter number of days : ";
    cin >> day;

    for (i = 0; i < day; i++)
    {
        cout << "Enter daily sales day (" << i + 1 << ") = ";
        cin >> sales;
        fout << "day (" << i + 1 << ") = " << sales << endl;
    }
    fout.close();

    return 0;
}