#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main()
{
	int i, day, sales;

	ofstream fout("C:/Users/Toloo Fajr/Desktop/C++ Course/f.txt",ios::app);
	if (!fout)
	{
		cerr << "File not open!" << endl;
		exit(1);
	}
	cout << "Enter number of days : ";
	cin >> day;

	for (i = 0; i < day; i++)
	{
		cout << "Enter daily sales day (" << i + 1 << ") = ";
		cin >> sales;
		fout << "day (" << i + 1 << ") = " << sales << endl;
	}
	fout.close();

	return 0;
}