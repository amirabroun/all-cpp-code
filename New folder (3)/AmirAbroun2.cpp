#include <iostream>
using namespace std;

void read(string[], int[], int[], int[]);
void write(string[], int[], int[]);
int BIG(int[]);
const int LEN = 3;

int main()
{
    string arr_FName[LEN];
    int arr_id[LEN];
    int arr_birth[LEN]; // just month
    int arr_birth_big[12];

    read(arr_FName, arr_birth, arr_id, arr_birth_big);
    int month = BIG(arr_birth_big);
    write(arr_FName, arr_id, arr_birth, month);
    return 0;
}

void read(string name[], int id[], int birth[], int age_big[])
{
    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-first name: ";
        cin >> name[i];
    }
    cout << endl;

    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-id: ";
        cin >> id[i];
    }
    cout << endl;

    for (int i = 0; i < 12; i++)
    {
        age_big[i] = 0;
    }

    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-month of Birth: ";
        cin >> birth[i];
        age_big[birth[i]]++;
    }
}
void write(string name[], int id[], int birth[], int big)
{
    for (int i = 0; i < LEN; i++)
    {
        if (birth[i] == big)
        {
            cout << i + 1 << ". name:" << name[i] << endl;
            cout << i + 1 << ". id:" << id[i] << endl;
            cout << i + 1 << ". birth:" << birth[i];
        }
    }
}

int BIG(int arr[])
{
    int big = 0;
    for (int i = 0; i < 12; i++)
    {
        if (arr[i] > big)
        {
            big = arr[i];
        }
    }
    return big;
}