#include <iostream>
using namespace std;

int BIG(int[], int);

int main()
{
    int n;
    cout << "N?";
    cin >> n;

    int arrNUM[n];
    int arrsum[n];
    for (int i = 0; i < n; i++)
    {
        arrsum[i] = 0;
    }

    for (int i = 0; i < n; i++)
    {
        cout << "enter number:";
        cin >> arrNUM[i];
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (arrNUM[i] == arrNUM[j])
            {
                arrsum[i]++;
            }
        }
    }

    cout << BIG(arrsum, n);
    return 0;
}

int BIG(int arr[], int n)
{
    int big = 0;
    for (int i = 0; i < n; i++)
    {
        if (arr[i] > big)
        {
            big = arr[i];
        }
    }
    return big;
}