// BY Amir Abroun
#include <iostream>

using namespace std;
int main()
{
    int line, num = 1;
    
    cout << "How many lines: ";
    cin >> line;
    int Space = line;

    do
    {
        for (int i = Space; i > 0; i--)
        {
            cout << " ";
        }

        for (int i = 0; i < (2 * num) - 1; i++)
        {
            cout << "*";
        }
        cout << endl;

        Space--;
        num++;
    } while ((line + 1) != num);

    return 0;
}