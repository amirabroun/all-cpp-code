#include <iostream>
#include <conio.h>
using namespace std;

int main()
{
    int i, j, k, x, n;

    cout << "Enter size matrise [n][n] : ";
    cin >> n;

    int a[n][n], b[n][n], c[n][n], d[n][n], h[n][n];

    cout << "\nEnter array matrix [1] : " << endl;

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            cout << "[ " << i + 1 << " ][ " << j + 1 << " ] = ";
            cin >> a[i][j];
        }
    }
    cout << "\nEnter array matrix [2] : " << endl;

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            cout << "[ " << i + 1 << " ][ " << j + 1 << " ] = ";
            cin >> b[i][j];
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            c[i][j] = a[i][j] + b[i][j];
            d[i][j] = a[i][j] - b[i][j];
        }
    }

    cout << "\nSum : " << endl;

    for (i = 0; i < n; i++)
    {
        cout << endl;
        for (j = 0; j < n; j++)
        {
            cout << '\t' << c[i][j];
        }
        cout << endl;
    }
    cout << "\n\n";

    cout << "Minuse : " << endl;

    for (i = 0; i < n; i++)
    {
        cout << endl;
        for (j = 0; j < n; j++)
        {
            cout << '\t' << d[i][j];
        }
        cout << endl;
    }

    cout << '\n';

    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
        {
            x = 0;
            for (k = 0; k < n; k++)
            {
                x += a[i][k] * b[k][j];
                h[i][j] = x;
            }
        }

    cout << "\nMultiple : " << endl;
    for (i = 0; i < n; i++)
    {
        cout << endl;
        for (j = 0; j < n; j++)
            cout << "\t" << h[i][j];
        cout << endl;
    }

    getch();
}