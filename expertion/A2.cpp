#include <iostream>
using namespace std;

int main()
{
  int i, j, rows;

  cout << "Number of rows : ";
  cin >> rows;

  cout << endl;
  for (i = 0; i <= rows; i++)
  {
    cout << "  ";
    for (j = i; j < rows; j++)
      cout << "   ";

    for (j = 0; j < (2 * i) - 1; j++)
    {

      cout << j + 1 << "  ";
    }
    cout << "\n\n\n";
  }

  return 0;
}