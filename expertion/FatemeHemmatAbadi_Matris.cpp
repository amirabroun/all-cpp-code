#include <iostream>
using namespace std;
int main()
{
    int n;
    cout << "n? ";
    cin >> n;
    int Total[n][n];
    int Submission[n][n];
    int Multiple[n][n];
    int array1[n][n];
    int array2[n][n];

    cout << "\narr1: \n";
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cout << i + 1 << "." << j + 1 << "  = ";
            cin >> array1[i][j];
        }
    }
    cout << "\narr2: \n";
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cout << i + 1 << "." << j + 1 << "  = ";
            cin >> array2[i][j];
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            Total[i][j] = array1[i][j] + array2[i][j];
            Submission[i][j] = array1[i][j] - array2[i][j];
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            int x = 0;
            for (int k = 0; k < n; k++)
            {
                x += array1[i][k] * array2[i][j];
                Multiple[i][j] = x;
            }
        }
    }
    cout << "\nTotal: ";
    for (int i = 0; i < n; i++)
    {
        cout << endl;
        for (int j = 0; j < n; j++)
        {
            cout << "   " << Total[i][j];
        }
        cout << endl;
    }
    cout << "\n\n";
    cout << "Submission: ";
    for (int i = 0; i < n; i++)
    {
        cout << endl;
        for (int j = 0; j < n; j++)
        {
            cout << "   " << Submission[i][j];
        }
        cout << endl;
    }
    cout << "\nMultiple : ";
    for (int i = 0; i < n; i++)
    {
        cout << endl;
        for (int j = 0; j < n; j++)
            cout << "   " << Multiple[i][j];
        cout << endl;
    }
    return 0;
}// Fteme Hemt