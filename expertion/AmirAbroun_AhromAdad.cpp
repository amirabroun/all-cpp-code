// BY Amir Abroun
#include <iostream>

using namespace std;
int main()
{
    int num;
    cout << "Number of: ";
    cin >> num;

    int line = 1;
    do
    {
        for (int i = 0; i < (2 * line) - 1; i++)
        {
            cout << i + 1 << " ";
        }
        cout << endl;

        line++;
    } while ((num + 1) != line);

    return 0;
}