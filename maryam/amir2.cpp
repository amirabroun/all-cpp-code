//by Amir Abroun
#include <iostream>
#include <string>

using namespace std;

string search(string[], int, string);
int search2(string[], string, int);
int control(int[], int);

int main()
{
    cout << "-LOG IN-";
    int N_vertex;
    cout << "\n\nHow many vertex? ";
    cin >> N_vertex;

    string A_vertex[N_vertex];
    for (int i = 0; i < N_vertex; i++)
    {
        cout << i + 1 << ". V(G) = ";
        cin >> A_vertex[i];
    }

    int N_edge;
    cout << "\nHow many edge? ";
    cin >> N_edge;

    int A_Degree[N_vertex];
    for (int i = 0; i < N_vertex; i++)
    {
        A_Degree[i] = 0;
    }

    string VERTEX1, VERTEX2;
    string arr_Head1[N_edge], arr_Head2[N_edge];
    string coloring[N_vertex];

    string A_edge[N_edge];
    for (int i = 0; i < N_edge; i++)
    {
        cout << i + 1 << ". E(G): \n";
        cout << "from: ";
        cin >> VERTEX1;

        int i1 = search2(A_vertex, VERTEX1, N_vertex);
        A_Degree[i1] += 1;

        cout << "to: ";
        cin >> VERTEX2;

        i1 = search2(A_vertex, VERTEX2, N_vertex);
        A_Degree[i1] += 1;

        string t1 = search(A_vertex, N_vertex, VERTEX1);
        string t2 = search(A_vertex, N_vertex, VERTEX2);

        arr_Head1[i] = VERTEX1;
        arr_Head2[i] = VERTEX2;

        A_edge[i] = t1 + t2;
        for (int j = 0; j < N_vertex; j++)
        {
            if (coloring[j] == "\0")
            {
                coloring[j] = "red";
                if (j != (N_vertex - 1))
                    coloring[j + 1] = "green";
            }
        }
        for (int j = 0; j < N_vertex; j++)
        {
            for (int k = 0; k < N_vertex; k++)
            {
                if (A_edge[j] == A_vertex[j] + A_vertex[k])
                {
                    if (coloring[j] == coloring[k])
                    {
                        coloring[j] = "blue";
                    }
                }
            }
        }
        cout << "\n";
    }
    cout << "-OUTPUT-";

    cout << "\n\nV(G) = {";
    for (int i = 0; i < N_vertex; i++)
    {
        cout << A_vertex[i];
        if (i != (N_vertex - 1))
            cout << ",";
    }
    cout << "}";

    cout << "\nE(G) = {";
    for (int i = 0; i < N_edge; i++)
    {
        cout << A_edge[i];
        if (i != (N_edge - 1))
            cout << ",";
    }
    cout << "}\n\n";

    for (int i = 0; i < N_vertex; i++)
    {
        cout << "Degree " << A_vertex[i] << ": " << A_Degree[i] << endl;
    }

    cout << endl;
    int complete_graph = (N_vertex * (N_vertex - 1)) / 2;

    if (N_edge == complete_graph)
        cout << "The graph is complete";
    else
    {
        cout << "The graph is not complete\n";
        int reg = control(A_Degree, N_vertex);
        if (reg >= 0)
        {
            cout << reg << "-regular graph\n\n";
        }
    }

    // for (int i = 0; i < N_vertex; i++)
    // {
    //     for (int j = 0; j < N_vertex; j++)
    //     {
    //         if (A_edge[i] == (A_vertex[j + 1] + A_vertex[j]) || A_edge[j] == (A_vertex[j] + A_vertex[j + 1]))
    //         {
    //             coloring[j] = "red";
    //             coloring[j + 1] = "green";
    //         }
    //     }
    // }

    for (int i = 0; i < N_vertex; i++)
    {
    }

    for (int i = 0; i < N_vertex; i++)
    {
        cout << "Vertex " << A_vertex[i] << ": " << coloring[i] << endl;
    }

    return 0;
}

string search(string a[], int n, string x)
{
    for (int i = 0; i < n; i++)
    {
        if (a[i] == x)
        {
            return x;
        }
    }

    return "Never show this!";
}

int search2(string ver[], string Head, int n)
{
    for (int i = 0; i < n; i++)
    {
        if (ver[i] == Head)
        {
            return i;
        }
    }

    return 0;
}

int control(int deg[], int ver)
{
    for (int i = 0; i < ver; i++)
    {
        if (deg[0] != deg[i])
        {
            return -1;
        }
    }

    return deg[1];
}
