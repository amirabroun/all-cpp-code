#include <iostream>
using namespace std;

int Equal(int[], const int);

int main()
{
    const int len = 100;
    int array[len];
    int n;

    cout << "Enter size : ";
    cin >> n;

    cout << "Enter elements array :\n";
    for (int i = 0; i < n; i++)
    {
        cout << "Array[" << i + 1 << "]= ";
        cin >> array[i];
    }

    cout << "\nResult : " << Equal(array, n);

    return 0;
}

int Equal(int a[], const int len)
{
    int i = 0, j = 1, flag = 0;

    while (j < len)
    {
        if (a[i] == a[j])
        {
            flag++;
        }
        j++;
    }
    if (flag == len - 1)
        return 1;
    return -1;
}