#include <iostream>
#include <string>
using namespace std;

string search(string[], int, string);
int search2(string[], string, int);

int main()
{
    int n_ras;
    cout << "Chand ras?";
    cin >> n_ras;

    int A_montazam[n_ras];
    for (int i = 0; i < n_ras; i++)
    {
        A_montazam[i] = 0;
    }

    string A_ras[n_ras];
    for (int i = 0; i < n_ras; i++)
    {
        cout << "A(" << i + 1 << ") = ";
        cin >> A_ras[i];
    }

    int n_yal;
    cout << "\nChand yal? ";
    cin >> n_yal;
    string A_yal[n_yal];

    string Head1, Head2;
    string arr_Head1[n_yal], arr_Head2[n_yal];

    for (int i = 0; i < n_yal; i++)
    {
        cout << "from: ";
        cin >> Head1;
        arr_Head1[i] = Head1;

        int i1 = search2(A_ras, Head1, n_ras);
        A_montazam[i1] += 1;

        cout << "to: ";
        cin >> Head2;
        arr_Head2[i] = Head2;
        int i2 = search2(A_ras, Head2, n_ras);
        A_montazam[i2] += 1;

        string t1 = search(A_ras, n_ras, Head1);
        string t2 = search(A_ras, n_ras, Head2);

        A_yal[i] = t1 + "," + t2;
    }

    for (int i = 0; i < n_ras; i++)
    {

        cout << A_montazam[i] << endl;
    }

    return 0;
}

string search(string a[], int n, string x)
{
    for (int i = 0; i < n; i++)
    {
        if (a[i] == x)
        {
            return x;
        }
    }
    return "error";
}

int search2(string ras[], string Head, int n)
{
    for (int i = 0; i < n; i++)
    {
        if (ras[i] == Head)
        {
            return i;
        }
    }
    return 0;
}
