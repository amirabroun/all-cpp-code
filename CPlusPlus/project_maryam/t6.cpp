#include <iostream>
#include <string>
using namespace std;

string search(string[], int, string);
int search2(string[], string[], string[], int);


int main()
{
    int n_ras;
    cout << "Chand ras?";
    cin >> n_ras;

    string A_ras[n_ras];
    for (int i = 0; i < n_ras; i++)
    {
        cout << "A(" << i + 1 << ") = ";
        cin >> A_ras[i];
    }

    int n_yal;
    cout << "\nChand yal?";
    cin >> n_yal;

    string A_yal_Head1[n_yal];
    string A_yal_Head2[n_yal];

    string A_yal[n_yal];
    string Head1, Head2;

    for (int i = 0; i < n_yal; i++)
    {
        cout << "from: ";
        cin >> Head1;
        A_yal_Head1[i] = Head1;

        cout << "to: ";
        cin >> Head2;
        A_yal_Head2[i] = Head2;

        string t1 = search(A_ras, n_ras, Head1);
        string t2 = search(A_ras, n_ras, Head2);

        A_yal[i] = t1 + "," + t2;
    }

    int A_montazam[n_ras];
    for (int i = 0; i < n_ras; i++)
    {
        A_montazam[i] = search2(A_yal_Head1, A_yal_Head2, A_ras, i);
        cout << A_montazam[i] << endl;
    }

    return 0;
}

string search(string a[], int n, string x)
{
    for (int i = 0; i < n; i++)
    {
        if (a[i] == x)
        {
            return x;
        }
    }
    return "error";
}

int search2(string Head1[], string Head2[], string ras[], int n)
{
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        if (ras[i] == Head1[i])
        {
            sum += 1;
        }
        if (Head2[i] == ras[i])
        {
            sum += 1;
        }
    }

    return sum;
}
