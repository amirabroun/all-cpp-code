#include <iostream>
#include <string>
using namespace std;

string search(string[], int, string);
int search2(string[], string, int);

int main()
{
    int N_vertex;
    cout << "How many vertex? ";
    cin >> N_vertex;

    string A_vertex[N_vertex];
    for (int i = 0; i < N_vertex; i++)
    {
        cout << i + 1 << ". V(G) = ";
        cin >> A_vertex[i];
    }

    int N_edge;
    cout << "\nHow many edge? ";
    cin >> N_edge;
    int A_montazam[N_vertex];
    for (int i = 0; i < N_vertex; i++)
    {
        A_montazam[i] = 0;
    }
    string VERTEX1, VERTEX2;
    string arr_Head1[N_edge], arr_Head2[N_edge];

    string A_edge[N_edge];
    for (int i = 0; i < N_edge; i++)
    {
        cout << i + 1 << ". E(G): \n";
        cout << "from: ";
        cin >> VERTEX1;

        int i1 = search2(A_vertex, VERTEX1, N_vertex);
        A_montazam[i1] += 1;

        cout << "to: ";
        cin >> VERTEX2;

        int i2 = search2(A_vertex, VERTEX2, N_vertex);
        A_montazam[i2] += 1;

        string t1 = search(A_vertex, N_vertex, VERTEX1);
        string t2 = search(A_vertex, N_vertex, VERTEX2);

        arr_Head1[i] = VERTEX1;
        arr_Head2[i] = VERTEX2;

        A_edge[i] = t1 + "," + t2;
        cout << "\n";
    }

    for (int i = 0; i < N_vertex; i++)
    {
        cout << "Degree " << A_vertex[i] << ": " << A_montazam[i] << endl;
    }

    return 0;
}

string search(string a[], int n, string x)
{
    for (int i = 0; i < n; i++)
    {
        if (a[i] == x)
        {
            return x;
        }
    }
    return "Never show this!";
}

int search2(string ras[], string Head, int n)
{
    for (int i = 0; i < n; i++)
    {
        if (ras[i] == Head)
        {
            return i;
        }
    }
    return 0;
}
