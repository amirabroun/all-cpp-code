#include <iostream>
using namespace std;

float min(int, int);

int main()
{
    float num1, num2;
    
    cout << "enter numbers: " << endl;
    cin >> num1 >> num2;
    
    cout << "\nyour minimum number is " << min(num1, num2);
    return 0;
}

float min(int num1, int num2)
{
    if (num1 < num2)
        return num1;
    else
        return num2;
}