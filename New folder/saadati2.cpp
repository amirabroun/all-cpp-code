#include <iostream>
using namespace std;

int main()
{
    int number, result = 0;
    while (1)
    {
        cout << "enter integer: ";
        cin >> number;

        if (number == 0)
        {
            break;
        }
        if (number >= result)
        {
            result = number;
        }
    }
    
    cout << "\n\n maximum number of: " << result;
    return 0;
}