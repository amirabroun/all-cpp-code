#include <iostream>
using namespace std;

int main()
{
    int num, unit, sum_num = 0, sum_unit = 0;
    for (int i = 0; i < 10; i++)
    {
        cout << "enter Score: ";
        cin >> num;

        cout << "how many unit: ";
        cin >> unit;

        num *= unit;
        sum_num += num;
        sum_unit += unit;

        cout << endl;
    }

    cout << "Average: " << sum_num / sum_unit;
    return 0;
}