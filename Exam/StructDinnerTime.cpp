#include <iostream>
using namespace std;

struct Time
{
    int hour;
    int minute;
    int second; 
};

void printMilitary(const Time&);
void printStandard(const Time&);

int main()
{
    Time dinnerTime;

    // set members value
    cout << "\nEnter the dinner time in military format: \n";
    cout << "hour = ";
    cin >> dinnerTime.hour;
    cout << "minute = ";
    cin >> dinnerTime.minute;
    cout << "second = ";
    cin >> dinnerTime.second;

    cout << "\nDinner will be held at ";
    printMilitary(dinnerTime);
    cout << " military time is, \nwhich is ";
    printStandard(dinnerTime);
    cout << " standard time. \n";

    return 0;
}

void printMilitary(const Time &t)
{
    cout << (t.hour < 10 ? "0" : "") << t.hour << ":";
    cout << (t.minute < 10 ? "0" : "") << t.minute << ":";
    cout << (t.second < 10 ? "0" : "") << t.second;
}

void printStandard(const Time &t)
{
    cout << ((t.hour == 0 || t.hour == 12) ? 12 : t.hour % 12);
    cout << ":" << (t.minute < 10 ? "0" : "") << t.minute;
    cout << ":" << (t.second < 10 ? "0" : "") << t.second;
    cout << (t.hour < 12 ? " AM" : " PM");
}
