#include <iostream>
using namespace std;

struct person
{
    char name[50];
    int age;
    long int id;
};

void print(person);

int main()
{
    person Amir;

    cout << "Name: ";
    cin.get(Amir.name, 50);

    cout << "Age: ";
    cin >> Amir.age;

    cout << "Id: ";
    cin >> Amir.id;

    print(Amir);

    return 0;
}

void print(person Amir)
{
    cout << "Your Info: " << endl;
    cout << Amir.name << endl;
    cout << Amir.age << endl;
    cout << Amir.id << endl;
}