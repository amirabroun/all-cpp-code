#include <iostream>
using namespace std;

int main()
{
    int x, y, *p1, *p2;
    p1 = 1000;
    p2 = 1024;
    y = 20;
    x = y / 2;
    p2 = &y;
    p1 = &x;
    cout << *p1 << *p2 << p1;
    p2 = p2 - 1;
    cout << p2;
    return 0;
}
