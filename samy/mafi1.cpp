#include <iostream>
using namespace std;

int birth(int, string);
const int LEN = 50;

int main()
{
    int arr_id[LEN];
    string arr_FName[LEN];
    string arr_LName[LEN];
    string arr_contry[LEN];
    int arr_birth[LEN];
    int arr_point[LEN];

    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-id: ";
        cin >> arr_id[i];
    }
    cout << endl;
    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-first name: ";
        cin >> arr_FName[i];
    }
    cout << endl;
    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-last name: ";
        cin >> arr_LName[i];
    }
    cout << endl;
    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-destination country: ";
        cin >> arr_contry[i];
    }
    cout << endl;
    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-Year of Birth: ";
        cin >> arr_birth[i];
    }
    cout << endl;
    for (int i = 0; i < LEN; i++)
    {
        cout << i + 1 << "-Undergraduate GPA: ";
        cin >> arr_point[i];
    }
    cout << endl;
    cout << "-OUT PUT-";
    for (int i = 0; i < LEN; i++)
    {
        int x = arr_birth[i];
        string y = arr_contry[i];
        
        if (birth(x, y) == 1)
        {
            cout << arr_id[i] << "-" << arr_FName[i] << endl;
        }
    }

    return 0;
}

int birth(int year, string contry)
{
    if (year >= 1370 && contry == "Australia")
    {
        return 1;
    }

    return 0;
}