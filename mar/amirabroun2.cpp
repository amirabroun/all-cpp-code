// BY Amir Abroun
#include <iostream>

using namespace std;
int main()
{
    int n;
    cout << "N*N. N: ";
    cin >> n;

    int Total = 0;
    int Multiplication = 0;

    cout << "\narr1: \n";
    int arr1[n][n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> arr1[i][j];
        }
    }

    cout << "\narr2: \n";
    int arr2[n][n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> arr2[i][j];
        }
    }

    // total
    for (int i = 0; i < n; i++)
    {
        Total += arr1[i][0] + arr2[i][0];
    }
    // Multiplication
    for (int i = 0; i < n; i++)
    {
        Multiplication += arr1[n-1][i] * arr1[n-1][i];
    }

    cout << "\nTotal: " << Total << endl;

    cout << "\n\n";

    cout << "\nMultiple: " << Multiplication << endl;

    return 0;
}