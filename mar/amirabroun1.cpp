#include <iostream>
using namespace std;

int main()
{
    int n;
    float t1 = 1, t2 = 5, t3;
    float sum;
    cout << "Enter the number of terms: ";
    cin >> n;

    cout << "Series: ";

    for (int i = 1; i <= n; ++i)
    {
        if (i == 1)
        {
            cout << t1;
            if (i != n)
                cout << " ,";
            continue;
        }
        if (i == 2)
        {
            cout << t2;
            if (i != n)
                cout << " ,";
            continue;
        }
        t3 = (t1 + t2) / 2;
        t1 = t2;
        t2 = t3;

        cout << t3;
        if (i != n)
            cout << " ,";
        if (i == n)
        {
            sum = t3;
        }
    }

    cout << "\nyour n: " << sum;

    return 0;
}
